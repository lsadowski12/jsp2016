<%@page import="com.example.projectJSP.domain.Smartphone"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update/Delete</title>
</head>
<style type="text/css">

.datagrid table { 
 border-collapse: collapse;
 text-align: left;
 width: 100%;
 } 
.datagrid {
font: normal 12px/150% Arial, Helvetica, sans-serif;
background: #f9fdff;
overflow: hidden;
border: 1px solid #006699;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px; 
}
.datagrid table td, .datagrid table th {
padding: 3px 10px; 
}
.datagrid table thead th {
background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #0481b1), color-stop(1, #0481b1) );
background-color:#006699; 
color:#FFFFFF; 
font-size: 15px; 
font-weight: bold;
border-left: 1px solid #0070A8;
} 
.datagrid table thead th:first-child {
 border: none;
 }
.datagrid table tbody td {
 color: #00496B;
 border-left: 1px solid #E1EEF4;
font-size: 12px;
font-weight: normal;
 }.
	#wrapper {
        width:450px;
        margin:0 auto;
        font-family:Verdana, sans-serif;
    }
    legend {
        color:#0481b1;
        font-size:16px;
        padding:0 10px;
        background:#fff;
        -moz-border-radius:4px;
        box-shadow: 0 1px 5px rgba(4, 129, 177, 0.5);
        padding:5px 10px;
        text-transform:uppercase;
        font-family:Helvetica, sans-serif;
        font-weight:bold;
    }
    fieldset {
        border-radius:4px;
        background: #fff; 
        background: -moz-linear-gradient(#fff, #f9fdff);
        background: -o-linear-gradient(#fff, #f9fdff);
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#fff), to(#f9fdff)); /
        background: -webkit-linear-gradient(#fff, #f9fdff);
        padding:20px;
        border-color:rgba(4, 129, 177, 0.4);
    }
    input[type="submit"]{
        background: #222;
        border: none;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.3);
        text-transform:uppercase;
        color: #eee;
        cursor: pointer;
        font-size: 15px;
        margin: 5px 0;
        padding: 5px 22px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-border-radius:4px;
        -webkit-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
        -moz-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
    }
</style>

<div class="datagrid">
<table >
        <thead>
            <tr>
            	<th>ID</th>
                <th>Brand</th>
                <th>Model</th>
                <th>YEAR</th>
                <th>COLOUR</th>
                <th>CAMERA</th>
                <th>SOFTWARE</th>
                <th>SCREEN</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        
<jsp:useBean id="storage" class="com.example.projectJSP.service.StorageService" scope="application" />
<tbody>
<%
	int id = 0;
	for (com.example.projectJSP.domain.Smartphone smartphone : storage.getAllSmartphones()) {
		id = smartphone.getId();
		%>
		<tr>
		<td>
		<%
		out.println("<p>" + smartphone.getId() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getBrand() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getModel() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getYear() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getColour() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getCamera() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getSoftware() + "</p>");
		%>
		</td>
		<td>
		<%
		out.println("<p>" + smartphone.getScreen() + "</p>");
		%>
		</td>
				
		<%
		
		out.println("<td><a href=\"deleteSmartphone?smartphoneId="+String.valueOf(smartphone.getId())+"\">Delete</a>");
		out.println("<a href=\"updateSmartphone.jsp?smartphoneId="+String.valueOf(smartphone.getId())+"\">Update</a></td></tr>");
	}
		%>
		</div>
	<p>
		<fieldset>
		<center>
			<form action=getSmartphoneData.jsp method="post">
   			<div><input type="submit" value="Add smartphone"></div>
			</form>

		</center>
		</fieldset>
	</p>

</body>
</html>