<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="refresh" content="0; URL=http://localhost:8080/projectJSP/showAllSmartphones.jsp">
<title>Smartphone</title>
</head>
<body>
<jsp:useBean id="smartphone" class="com.example.projectJSP.domain.Smartphone" scope="session" />

<jsp:setProperty name="smartphone" property="*" /> 

<jsp:useBean id="storage" class="com.example.projectJSP.service.StorageService" scope="application" /> 
<%
  storage.update(smartphone.getId(),smartphone);
  
%>


</body>
</html>