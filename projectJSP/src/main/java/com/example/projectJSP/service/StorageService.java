package com.example.projectJSP.service;

import java.util.ArrayList;
import java.util.List;

import com.example.projectJSP.domain.Smartphone;



public class StorageService {
	
	private List<Smartphone> db = new ArrayList<Smartphone>();
	
	public void add(Smartphone smartphone){
		Smartphone newSmartphone = new Smartphone
				(
				Smartphone.lastID,
				smartphone.getBrand(),
				smartphone.getYear(),
				smartphone.getModel(),
				smartphone.getColour(),
				smartphone.getCamera(),
				smartphone.getSoftware(),
				smartphone.getScreen()
				);
		db.add(newSmartphone);
		Smartphone.lastID++;
	}
	
	public List<Smartphone> getAllSmartphones(){
		return db;
	}
	
	public void delete(int id){
		
		int i=0;
		while(db.get(i).id != id)
		i++;

		db.remove(i);
	}
	public void update(int id, Smartphone smartphone){
		int index = -1;
		for(Smartphone smartphone1 : db){
			if(id == smartphone1.getId()){
				index = db.indexOf(smartphone1);
				break;
			}
		}
		Smartphone newSmartphone = new Smartphone(smartphone.getId(), smartphone.getBrand(), smartphone.getYear(),smartphone.getModel(), smartphone.getColour(), smartphone.getCamera(), smartphone.getSoftware(), smartphone.getScreen());
		db.set(index, newSmartphone);
	}	
	

	
}
