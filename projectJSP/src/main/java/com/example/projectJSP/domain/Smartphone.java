//calosc model
package com.example.projectJSP.domain;

public class Smartphone {
	
	private String brand = "";
	private int year = 2015;
	private String model = "";
	private String colour = "";
	private String camera = "";
	private String software = "";
	private String screen = "";
	public int id = 0;
	public static int lastID = 0;
	
	
	public Smartphone() {
		super();
	}
	//konstrikor
	public Smartphone(int id,String brand, int year, String model,String colour,String camera, String software, String screen) {
		super();
		this.id = id;
		this.brand = brand;
		this.year = year;
		this.model = model;
		this.colour = colour;
		this.camera = camera;
		this.software = software;
		this.screen = screen;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getModel(){
		return model;
	}
	public void setModel(String model){
		this.model=model;
	}
	public String getColour(){
		return colour;
	}
	public void setColour(String colour){
		this.colour = colour;
	}
	public String getCamera(){
		return camera;
	}
	public void setCamera(String camera){
		this.camera = camera;
	}
	public String getSoftware(){
		return software;
	}
	public void setSoftware(String software){
		this.software = software;
	}
	public String getScreen(){
		return screen;
	}
	public void setScreen(String screen){
		this.screen = screen;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
}


