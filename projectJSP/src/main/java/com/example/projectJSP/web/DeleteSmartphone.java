package com.example.projectJSP.web;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.example.projectJSP.domain.Smartphone;

import com.example.projectJSP.service.StorageService;

@WebServlet(urlPatterns = "/deleteSmartphone")

public class DeleteSmartphone extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		
		StorageService ss = (StorageService) getServletContext().getAttribute("storage");
		
		if(request.getSession().getAttribute("smartphone") == null){
			request.getSession().setAttribute("smartphone", new Smartphone());	
		}
		
		int ID = Integer.parseInt(request.getParameter("smartphoneId"));
         
		ss.delete(ID);

		writer.println("<a href=\"showAllSmartphones.jsp\">Show all smartphones</a>");
		
		writer.close();
	}
		@Override
		public void init() throws ServletException {
			if(getServletContext().getAttribute("storage") == null)
			{
				getServletContext().setAttribute("storage", new StorageService());
			}
		}
}
