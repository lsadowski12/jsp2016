package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n");
      out.write("   \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>CRUD application</title>\n");
      out.write("    </head>\n");
      out.write("    <style type=\"text/css\">\n");
      out.write("    \n");
      out.write("    #wrapper {\n");
      out.write("        width:450px;\n");
      out.write("        margin:0 auto;\n");
      out.write("        font-family:Verdana, sans-serif;\n");
      out.write("    }\n");
      out.write("    legend {\n");
      out.write("        color:#0481b1;\n");
      out.write("        font-size:16px;\n");
      out.write("        padding:0 10px;\n");
      out.write("        background:#fff;\n");
      out.write("        -moz-border-radius:4px;\n");
      out.write("        box-shadow: 0 1px 5px rgba(4, 129, 177, 0.5);\n");
      out.write("        padding:5px 10px;\n");
      out.write("        text-transform:uppercase;\n");
      out.write("        font-family:Helvetica, sans-serif;\n");
      out.write("        font-weight:bold;\n");
      out.write("    }\n");
      out.write("    fieldset {\n");
      out.write("        border-radius:4px;\n");
      out.write("        background: #fff; \n");
      out.write("        background: -moz-linear-gradient(#fff, #f9fdff);\n");
      out.write("        background: -o-linear-gradient(#fff, #f9fdff);\n");
      out.write("        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#fff), to(#f9fdff)); /\n");
      out.write("        background: -webkit-linear-gradient(#fff, #f9fdff);\n");
      out.write("        padding:20px;\n");
      out.write("        border-color:rgba(4, 129, 177, 0.4);\n");
      out.write("    }\n");
      out.write("    input[type=\"submit\"]{\n");
      out.write("        background: #222;\n");
      out.write("        border: none;\n");
      out.write("        text-shadow: 0 -1px 0 rgba(0,0,0,0.3);\n");
      out.write("        text-transform:uppercase;\n");
      out.write("        color: #eee;\n");
      out.write("        cursor: pointer;\n");
      out.write("        font-size: 15px;\n");
      out.write("        margin: 5px 0;\n");
      out.write("        padding: 5px 22px;\n");
      out.write("        -moz-border-radius: 4px;\n");
      out.write("        border-radius: 4px;\n");
      out.write("        -webkit-border-radius:4px;\n");
      out.write("        -webkit-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("        -moz-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("        box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("    }\n");
      out.write("    </style>\n");
      out.write("    <body>\n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("    \t<fieldset>\n");
      out.write("        <center>\n");
      out.write("       \t\t<legend>CRUD SMARTPHONE SERVICE</legend>\n");
      out.write("       \t\t<form action=getSmartphoneData.jsp method=\"post\">\n");
      out.write("        \t<input type=\"submit\" value=\"Add smartphone\">\n");
      out.write("       \t\t</form>\n");
      out.write("        \n");
      out.write("       \t\t<form action=showAllSmartphones.jsp method=\"post\">\n");
      out.write("        \t<div><input type=\"submit\" value=\"Select smartphones\"></div>\n");
      out.write("        \t</form>\n");
      out.write("        \t</center>\n");
      out.write("        </fieldset>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
