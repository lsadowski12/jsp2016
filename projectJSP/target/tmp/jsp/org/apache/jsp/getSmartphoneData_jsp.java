package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class getSmartphoneData_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      com.example.projectJSP.domain.Smartphone smartphone = null;
      synchronized (session) {
        smartphone = (com.example.projectJSP.domain.Smartphone) _jspx_page_context.getAttribute("smartphone", PageContext.SESSION_SCOPE);
        if (smartphone == null){
          smartphone = new com.example.projectJSP.domain.Smartphone();
          _jspx_page_context.setAttribute("smartphone", smartphone, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("<title>Smartphones</title>\n");
      out.write("</head>\n");
      out.write("<style type=\"text/css\">\n");
      out.write("    #wrapper {\n");
      out.write("        width:450px;\n");
      out.write("        margin:0 auto;\n");
      out.write("        font-family:Verdana, sans-serif;\n");
      out.write("    }\n");
      out.write("    legend {\n");
      out.write("        color:#0481b1;\n");
      out.write("        font-size:16px;\n");
      out.write("        padding:0 10px;\n");
      out.write("        background:#fff;\n");
      out.write("        -moz-border-radius:4px;\n");
      out.write("        box-shadow: 0 1px 5px rgba(4, 129, 177, 0.5);\n");
      out.write("        padding:5px 10px;\n");
      out.write("        text-transform:uppercase;\n");
      out.write("        font-family:Helvetica, sans-serif;\n");
      out.write("        font-weight:bold;\n");
      out.write("    }\n");
      out.write("    fieldset {\n");
      out.write("        border-radius:4px;\n");
      out.write("        background: #fff; \n");
      out.write("        background: -moz-linear-gradient(#fff, #f9fdff);\n");
      out.write("        background: -o-linear-gradient(#fff, #f9fdff);\n");
      out.write("        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#fff), to(#f9fdff)); /\n");
      out.write("        background: -webkit-linear-gradient(#fff, #f9fdff);\n");
      out.write("        padding:20px;\n");
      out.write("        border-color:rgba(4, 129, 177, 0.4);\n");
      out.write("    }\n");
      out.write("    input{\n");
      out.write("        color: #373737;\n");
      out.write("        background: #fff;\n");
      out.write("        border: 1px solid #CCCCCC;\n");
      out.write("        color: #aaa;\n");
      out.write("        font-size: 14px;\n");
      out.write("        line-height: 1.2em;\n");
      out.write("        margin-bottom:15px;\n");
      out.write("\n");
      out.write("        -moz-border-radius:4px;\n");
      out.write("        -webkit-border-radius:4px;\n");
      out.write("        border-radius:4px;\n");
      out.write("        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.2);\n");
      out.write("    }\n");
      out.write("    input[type=\"text\"]{\n");
      out.write("        padding: 8px 6px;\n");
      out.write("        height: 22px;\n");
      out.write("        width:280px;\n");
      out.write("    }\n");
      out.write("    input[type=\"text\"]:focus{\n");
      out.write("        background:#f5fcfe;\n");
      out.write("        text-indent: 0;\n");
      out.write("        z-index: 1;\n");
      out.write("        color: #373737;\n");
      out.write("        -webkit-transition-duration: 400ms;\n");
      out.write("        -webkit-transition-property: width, background;\n");
      out.write("        -webkit-transition-timing-function: ease;\n");
      out.write("        -moz-transition-duration: 400ms;\n");
      out.write("        -moz-transition-property: width, background;\n");
      out.write("        -moz-transition-timing-function: ease;\n");
      out.write("        -o-transition-duration: 400ms;\n");
      out.write("        -o-transition-property: width, background;\n");
      out.write("        -o-transition-timing-function: ease;\n");
      out.write("        width: 380px;\n");
      out.write("        \n");
      out.write("        border-color:#ccc;\n");
      out.write("        box-shadow:0 0 5px rgba(4, 129, 177, 0.5);\n");
      out.write("        opacity:0.6;\n");
      out.write("    }\n");
      out.write("    input[type=\"submit\"]{\n");
      out.write("        background: #222;\n");
      out.write("        border: none;\n");
      out.write("        text-shadow: 0 -1px 0 rgba(0,0,0,0.3);\n");
      out.write("        text-transform:uppercase;\n");
      out.write("        color: #eee;\n");
      out.write("        cursor: pointer;\n");
      out.write("        font-size: 15px;\n");
      out.write("        margin: 5px 0;\n");
      out.write("        padding: 5px 22px;\n");
      out.write("        -moz-border-radius: 4px;\n");
      out.write("        border-radius: 4px;\n");
      out.write("        -webkit-border-radius:4px;\n");
      out.write("        -webkit-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("        -moz-box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("        box-shadow: 0px 1px 2px rgba(0,0,0,0.3);\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("    .small {\n");
      out.write("        line-height:14px;\n");
      out.write("        font-size:12px;\n");
      out.write("        color:#999898;\n");
      out.write("        margin-bottom:3px;\n");
      out.write("    }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\n");
      com.example.projectJSP.service.StorageService storage = null;
      synchronized (application) {
        storage = (com.example.projectJSP.service.StorageService) _jspx_page_context.getAttribute("storage", PageContext.APPLICATION_SCOPE);
        if (storage == null){
          storage = new com.example.projectJSP.service.StorageService();
          _jspx_page_context.setAttribute("storage", storage, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write("\n");
      out.write("\n");
      out.write("<div id=\"wrapper\">\n");
      out.write("<form action=\"addSmartphone.jsp\" method=\"post\">\n");
      out.write("<fieldset>\n");
      out.write("   <legend>Smartphone Form</legend>\n");
      out.write("\n");
      out.write("  \t<div><input type=\"text\" name=\"brand\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.brand}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"Brand\"/><br /></div>\n");
      out.write(" \t<div> <input type=\"text\"  name=\"model\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.model}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"MODEL\"/><br /></div>\n");
      out.write(" \t<div><input type=\"text\"  name=\"year\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.year}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"YEAR\"/><br /></div>\n");
      out.write("   \t<div><input type=\"text\"  name=\"colour\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.colour}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"COLOUR\"/><br /></div>\n");
      out.write("  \t<div><input type=\"text\"  name=\"camera\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.camera}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"CAMERA\"/><br /></div>\n");
      out.write(" \t<div><input type=\"text\"  name=\"software\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.software}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"SOFTWARE\"/><br /></div>\n");
      out.write(" \t<div> <input type=\"text\"  name=\"screen\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${smartphone.screen}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" placeholder=\"SCREEN\"/><br /></div>\n");
      out.write("  <input type=\"submit\" value=\" OK \">\n");
      out.write("\n");
      out.write("</fieldset>  \n");
      out.write("\n");
      out.write("</form>\n");
      out.write("\n");
      out.write(" </div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
